#Tankbuster

This is a small Addon I wrote as a learning process for learning Arma 3 Config editing. This Addon contains a Modification of the AS50 Anti material rifle, which was
modified to shoot 40mm AP Rounds. The model as of right now has been Taken from the CUP Weapon Pack.

This Addon may be removed in the future, or improved upon.

#Requirements

* CUP Weapon Pack

The CUP Weapon pack is needed for the model. Once find a better model, this requirement will change.

#License
This project is licensed under the MIT License which can be found in the licenses folder.
