#Godmode

Godmode is an Addon that was initially based on an old version of Ares.

#Requirements
Godmode requires the following Addons present for it to fully work.

* Comunity Base Addons 3 (CBA_A3)
* Advanced Combat Environment 3 (ACE3)
* AllInArma TerrainPack (AiA TP)

These are the base requirements, and there may be more functionality that relies on other addons, but is not required for the most part of this addon.

Godmode works in Vanilla + CBA_A3 Arma, but may be reduced to a lower feature set since some modules and functions make use of RHS and ACE content.
A example of this is the Add Vehicle Weapons module, which besides Vanilla guns also contains RHS vehicle weapons.

ACE3 is being used for providing the Control Panel option in the ACE interaction menu and to make sure some that Taken over units don't fall unconcious.
Both of this is optional. If ACE is not present, then a traditional AddAction is going to be used to provide the access to the control panel.

RHS is used to provide Weapon / Vehicle / Ammo Classes, which may be used by modules such as "Add Vehicle Weapons" which supports RHS Guns.

#License
This project is licensed under the Creative Commons 4 License (CC-BY-NC-SA-4) License which can be found in the licenses folder.

This is due the fact that this project has been Started as a fork from Ares, which makes use of this license and thus this fork is bound to it aswell.

#Credits
Since this project has been using Ares as a base, I have to thank Astruyk for his Addon which has been an excellent base for this mod to exist.
You can check out his project here: https://github.com/astruyk/Ares
