#RHS Vanilla Launchers

Since RHS's Launchers don't work well with Vanilla Armor, because they decided to make their own special snowflake Armor System, This system makes certain RHS Launchers compatible with the Vanilla Armor System.

Keep in mind that this addon does not modify existing Launchers, but rather adds a new Launcher for engaging Vanilla Armor. This allows for the RHS launchers to still be compatible with the Vanilla Armor System.

#Requirements
* RHS: Escalation

#License
This project is licensed under the MIT License which can be found in the licenses folder.
