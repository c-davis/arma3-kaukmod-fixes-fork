#HuntIR Patches
This Addon provides the ability to use the HuntIR grenade launcher rounds with various mod weapons.

This Addon is tailored towards the FP modpack and is included in it. That is why there is a big amount of Addons requied for this addon.
You can modify the contents of this addon to only use the configs for the weapons you want.

#Requirements
* CUP Weapon Pack
* RHS: Escalation
* RH Weapon Pack
* R3F Famas

#License
This project is licensed under the MIT License which can be found in the licenses folder.
