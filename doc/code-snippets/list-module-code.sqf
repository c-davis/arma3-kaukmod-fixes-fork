//Description left, value right
_options = [
  ["",""],
  ["",""],
  ["",""],
  ["",""],
  ["",""],
  ["",""],
  ["",""],
  ["",""],
  ["",""]
];

//Push names in independent array
_optionNames = [];
{
	_optionNames pushBack (_x select 0);
} forEach _options;

//Dialogie Menu
_dialogResult = [
	"Test Dialogue",
	[
		["Option", _optionNames]
	]
] call Ares_fnc_ShowChooseDialog;
