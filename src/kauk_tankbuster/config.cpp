class CfgPatches
{
	class kauk_tankbuster
	{
		units[] = {};
		weapons[] = {"kauk_bstr"};
		author[] = {"Kaukassus"};
		requiredVersion = 1.32;
		requiredAddons[] = {"A3_Weapons_F"};
	};
};

class CfgWeapons {

	class Rifle_Base_F;
  class Rifle_Long_Base_F : Rifle_Base_F {};
  class CUP_srifle_AS50 : Rifle_Long_Base_F {};

  class kauk_bstr : CUP_srifle_AS50 {
    magazines[] = {"40Rnd_40mm_APFSDS_Tracer_Red_shells"};
		descriptionshort = "A930 ATB";
		displayName = "A930 ATB";
		author = "Kaukassus";
		reloadTime = 4.0;
  };
};
