class CfgPatches
{
	class kauk_fortifications
	{
		units[] = {
      "kauk_Land_fort_artillery_nest",
      "kauk_Land_fort_artillery_nest_EP1",
      "kauk_Land_fort_bagfence_corner",
      "kauk_Land_fort_bagfence_long",
      "kauk_Land_fort_bagfence_round",
      "kauk_Land_fort_rampart",
      "kauk_Land_fort_rampart_EP1",
      "kauk_Fort_Barracks_USMC",
      "kauk_Fort_Barricade",
      "kauk_Fort_EnvelopeBig",
      "kauk_Fort_EnvelopeBig_EP1",
      "kauk_Fort_EnvelopeSmall",
      "kauk_Fort_EnvelopeSmall_EP1",
      "kauk_Fortress1",
      "kauk_Fortress2",
      "kauk_Hedgehog",
      "kauk_WarfareBDepot",
      "kauk_WarfareBCamp",
      "kauk_Hhedgehog_concrete",
      "kauk_Hhedgehog_concreteBig",
      "kauk_Land_tent_east",
      "kauk_Camp",
      "kauk_CampEast"
    };
		weapons[] = {};
		author[] = {"Kaukassus"};
		requiredVersion = 1.32;
		requiredAddons[] = {};
	};
};

class CfgVehicleClasses
{
	class KaukFortifications
	{
		displayName = "Kauk Objects";
	};
};

class CfgVehicles
{
  class House;
  class Strategic;
  class Camp_base : Strategic {};
  class Land_fort_artillery_nest : House {};
  class Land_fort_artillery_nest_EP1 : Land_fort_artillery_nest {};
  class Land_fort_bagfence_corner : House {};
  class Land_fort_bagfence_long : House {};
  class Land_fort_bagfence_round : House {};
  class Land_fort_rampart : House {};
  class Land_fort_rampart_EP1 : House {};
  class Hedgehog : Strategic {};
  class WarfareBBaseStructure : House {};
  class Fort_Barracks_USMC : WarfareBBaseStructure {};
  class Fort_Barricade : Strategic {};
  class Fort_EnvelopeBig : House {};
  class Fort_EnvelopeBig_EP1 : Fort_EnvelopeBig {};
  class Fort_EnvelopeSmall : House {};
  class Fort_EnvelopeSmall_EP1 : Fort_EnvelopeSmall {};
  class Fortress1 : Strategic {};
  class Fortress2 : Fortress1 {};
  class WarfareBDepot : WarfareBBaseStructure {};
  class WarfareBCamp : Fortress1 {};
  class Hhedgehog_concrete : Strategic {};
  class Hhedgehog_concreteBig : Hhedgehog_concrete {};
  class Land_tent_east : House {};
  class Camp : Camp_base {};
  class CampEast : Camp_base {};
  class kauk_Land_fort_artillery_nest : Land_fort_artillery_nest {vehicleClass = "KaukFortifications";scopeCurator = 2;};
  class kauk_Land_fort_artillery_nest_EP1 : Land_fort_artillery_nest_EP1 {vehicleClass = "KaukFortifications";scopeCurator = 2;};
  class kauk_Land_fort_bagfence_corner : Land_fort_bagfence_corner {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Land_fort_bagfence_long : Land_fort_bagfence_long {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Land_fort_bagfence_round : Land_fort_bagfence_round {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Land_fort_rampart : Land_fort_rampart {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Land_fort_rampart_EP1 : Land_fort_rampart_EP1 {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Hedgehog : Hedgehog {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_Barracks_USMC : Fort_Barracks_USMC {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_Barricade : Fort_Barricade {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_EnvelopeBig : Fort_EnvelopeBig {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_EnvelopeBig_EP1 : Fort_EnvelopeBig_EP1 {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_EnvelopeSmall : Fort_EnvelopeSmall {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fort_EnvelopeSmall_EP1 : Fort_EnvelopeSmall_EP1 {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fortress1 : Fortress1 {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Fortress2 : Fortress2 {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_WarfareBDepot : WarfareBDepot {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_WarfareBCamp : WarfareBCamp {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Hhedgehog_concrete : Hhedgehog_concrete {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Hhedgehog_concreteBig : Hhedgehog_concreteBig {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Land_tent_east : Land_tent_east {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_Camp : Camp {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
  class kauk_CampEast : CampEast {vehicleClass = "KaukFortifications"; scopeCurator = 2;};
}