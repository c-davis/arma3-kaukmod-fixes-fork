class CfgPatches
{
	class kauk_rhs_vanilla_launchers
	{
		units[] = {};
		weapons[] = {"kauk_rhs_weap_rpg7"};
		author[] = {"Kaukassus"};
		requiredVersion = 1.32;
		requiredAddons[] = {"A3_Weapons_F"};
	};
};

class CfgWeapons {
	class Default;
	class LauncherCore : Default {};
	class Launcher : LauncherCore {};
	class Launcher_Base_F : Launcher {};
	class rhs_weap_rpg7 : Launcher_Base_F {};

  class launch_O_Titan_F;
  class rhs_weap_fgm148 : launch_O_Titan_F {};
  class rhs_weap_fim92 : launch_O_Titan_F {};

  class kauk_rhs_weap_fim92 : rhs_weap_fim92 {
    author = "Kaukassus";
    displayName = "FIM-92F (Vanilla)";
    magazines[] = {"Titan_AA"};
  };

  //Javelin Launcher
  class kauk_rhs_weap_fgm148 : rhs_weap_fgm148 {
    author = "Kaukassus";
    displayName = "FGM-148 Javelin (Vanilla)";
    magazines[] = {"Titan_AT","Titan_AP"};
    ace_javelin_enabled = 1;
    ace_overpressure_angle = 30;
    ace_overpressure_damage = 0.5;
    ace_overpressure_range = 2;
    ace_reloadlaunchers_enabled = 1;
    canLock = 1;
  };

  //RPG7 Version for engaging vanilla vehicles
	class kauk_rhs_weap_rpg7 : rhs_weap_rpg7 {
		author = "Kaukassus";
		displayName = "RPG-7V2 (Vanilla)";
		magazines[] = {"kauk_RPGAP_mag", "kauk_RPGHE_mag"};
		baseWeapon = "kauk_rhs_weap_rpg7";
	};
};

class CfgMagazines {
	class CA_LauncherMagazine;
  class RPG32_F : CA_LauncherMagazine {};

  class kauk_RPGAP_mag : RPG32_F {
    author = "Kaukassus";
    displayName = "PG-7V (Vanilla)";
    ammo = "kauk_RPGAP";
    model = "\rhsafrf\addons\rhs_weapons\rpg7\magazines\rhs_pg7vl_mag";
    modelSpecial = "\rhsafrf\addons\rhs_weapons\rpg7\rhs_pg7vl_loaded";
    picture = "\rhsafrf\addons\rhs_weapons\icons\pg7vl_ca.paa";
  };
  class kauk_RPGHE_mag : RPG32_F {
    author = "Kaukassus";
    displayName = "TBG-7V (Vanilla)";
    ammo = "kauk_RPGHE";
    model = "\rhsafrf\addons\rhs_weapons\rpg7\magazines\rhs_tbg7v_mag";
    modelSpecial = "\rhsafrf\addons\rhs_weapons\rpg7\rhs_tbg7v_loaded";
    picture = "\rhsafrf\addons\rhs_weapons\icons\tbg7v_ca.paa";
  };
};

class CfgAmmo {
	class R_PG32V_F;
  class R_TBG32V_F : R_PG32V_F {};
  class kauk_RPGAP : R_PG32V_F {
    model = "\rhsafrf\addons\rhs_weapons\rpg7\projectiles\pg7vr";
    soundHit[] = {"A3\Sounds_F\arsenal\weapons\Launchers\RPG32\RPG32_Hit",3.16228,1,1800};
  };
  class kauk_RPGHE : R_TBG32V_F {
    model = "\rhsafrf\addons\rhs_weapons\rpg7\projectiles\tbg7v";
    soundHit[] = {"A3\Sounds_F\arsenal\weapons\Launchers\RPG32\RPG32_Hit",3.16228,1,1800};
  };
};
