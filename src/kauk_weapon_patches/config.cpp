class CfgPatches {
	class kauk_weapon_patches {
		units[] = {};
		weapons[] = {};
		author[] = {"Kaukassus"};
		requiredVersion = 1.32;
		requiredAddons[] = {"r3f_armes_c", "asdg_jointrails", "A3_Weapons_F","ace_huntir"};
	};
};

class CfgWeapons {
	class UGL_F;
	class Rifle_Base_F;
	class arifle_MX_Base_F : Rifle_Base_F {};


	class Item_Base_F;

	class Item_Laserdesignator : Item_Base_F {
		visionMode[] = {"Normal","NVG", "Ti"};
	};


	#include "HuntIR\huntir.hpp"
};

#include "riflegrenades\r3f_riflegrenades.hpp"