//Robert Hammers M16's and M4's
class RH_m4 : Rifle_Base_F {
	class  M203 : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
};

//CUP M16
class CUP_arifle_M16_Base : Rifle_Base_F {
	class M203 : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
};

//CUP SCAR
class CUP_arifle_SCAR_Base : Rifle_Base_F {
	class EGLMMuzzle : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
};

//CUP XM8
class CUP_arifle_XM8_Base : Rifle_Base_F {
	class XM320Muzzle : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
};

//CUP L85
class CUP_arifle_L85A2_Base : Rifle_Base_F {
	class BAF_L17_40mm : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	}
};

//RHS M4's
class rhs_weap_m4_Base : arifle_MX_Base_F {
	class M203_GL : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
	class M320_GL : M203_GL {
		magazines[] += {};
	};
};

//RHS AK's
class rhs_weap_ak74m_Base_F : Rifle_Base_F {
	class GP25Muzzle : UGL_F {
		magazines[] += {"ACE_HuntIR_M203"};
	};
};
