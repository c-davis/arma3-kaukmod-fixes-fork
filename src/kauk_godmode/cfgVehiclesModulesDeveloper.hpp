class Kauk_Module_Dev_Debug_Console : Kauk_Developer_Module_Base {
	scopeCurator = 2;
	displayName = "Debug at Pos";
	function = "Kauk_fnc_DebugConsole";
};

class Kauk_Module_Dev_Open_View : Kauk_Developer_Module_Base {
	scopeCurator = 2;
	displayName = "Open Dev View";
	function = "Kauk_fnc_OpenViewerMenu";
};

class Kauk_Module_Dev_Get_Object_Class : Kauk_Developer_Module_Base {
	scopeCurator = 2;
	displayName = "Show Object Class";
	function = "Kauk_fnc_GetObjectClass";
};

class Kauk_Module_Dev_Get_List_Variables : Kauk_Developer_Module_Base {
	scopeCurator = 2;
	displayName = "Show Object Variables";
	function = "Kauk_fnc_ListVariables";
};
