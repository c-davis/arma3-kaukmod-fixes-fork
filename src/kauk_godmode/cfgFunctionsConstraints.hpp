class ConstraintsModules {
	file = "\kauk_godmode\modules\Constraints";
	class SetTexture;
	class AttachToObject;
	class DetachObject;
	class AttachRope;
	class DetachRopes;
	class ToggleDamage;
	class PushObject;
	class UnlimitedAmmo;
	class ToggleSimulation;
	class AttachToObjectWithRelease;
	class ChangePlayerSides;
};
