class Kauk {
	// Functions to call during initialization. See https://community.bistudio.com/wiki/Functions_Library_(Arma_3)
	class init {
		file = "\kauk_godmode\functions\init";
		class RespawnEvent;
		class InitGodmode { preInit = 1; };
	};

	// Functions called in response to events
	class events {
		file = "\kauk_godmode\functions\events";
		class OnModuleTreeLoad;
		class HandleCuratorObjectPlaced;
	};

	class ui {
		file = "\kauk_godmode\functions\UI";
		class CuratorPropertyActions;
		class ControlPanelActions;
		class AlignAttachment;
		class ShowChooseDialog;
		class DebugPos;
	};

	// Helper functions
	class util {
		file = "\kauk_godmode\functions";
		class AddUnitsToCurator;
		class BroadcastCode;
		class GetArrayDataFromUser;
		class GetGroupUnderCursor;
		class GetSafePos;
		class GetUnitUnderCursor;
		class GlobalExecute;
		class IsZeus;
		class MakePlayerInvisible;
		class MonitorCuratorDisplay;
		class ObjectsGrabber;
		class ObjectsMapper;
		class SearchBuilding;
		class TeleportPlayers;
		class ZenOccupyHouse;
		class ZeusRefresh;
		class ToggleReviveSystem;
		class SetAngle;
		class CreateLogic;
		class RemoteControl;
		class SwitchPlayer;
		class RefreshCuratorHandlers;
	};

	// Functions to perform module actions
	class modules {
		file = "\kauk_godmode\modules";
		class Empty {};
	};

	#include "cfgFunctionsArsenal.hpp"
	#include "cfgFunctionsBehaviour.hpp"
	#include "cfgFunctionsEquipment.hpp"
	#include "cfgFunctionsSaveLoad.hpp"
	#include "cfgFunctionsSpawn.hpp"
	#include "cfgFunctionsTeleport.hpp"
	#include "cfgFunctionsUtil.hpp"

	//Custom Categories
	#include "cfgFunctionsDeveloper.hpp"
	#include "cfgFunctionsAICommands.hpp"
	#include "cfgFunctionsConstraints.hpp"
};
