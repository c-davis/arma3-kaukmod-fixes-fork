class Kauk_Module_AI_ToggleAIAnim : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Toggle AI Brain";
	function = "Kauk_fnc_ToggleAIAnim";
};

class Kauk_Module_AI_UnitForceFire : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Unit Force Fire";
	function = "Kauk_fnc_UnitForceFire";
};

class Kauk_Module_AI_VehicleForceFire : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Vehicle Force Fire";
	function = "Kauk_fnc_VehicleForceFire";
};

class Kauk_Module_AI_UnitLook : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Unit Look At Pos";
	function = "Kauk_fnc_UnitLook";
};

class Kauk_Module_AI_UnitLookObject : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Unit Look At Object";
	function = "Kauk_fnc_UnitLookObject";
};

class Kauk_Module_AI_SelectAIWeapon : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Select AI Weapon";
	function = "Kauk_fnc_SelectAIWeapon";
};

class Kauk_Module_AI_UnitWatch : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Unit Watch Pos";
	function = "Kauk_fnc_UnitWatch";
};

class Kauk_Module_AI_UnitWatchStop : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Unit Watch Pos Stop";
	function = "Kauk_fnc_UnitWatchStop";
};

class Kauk_Module_AI_ArtilleryFire : Kauk_AICommands_Module_Base {
	scopeCurator = 2;
	displayName = "Artillery Fire Command";
	function = "Kauk_fnc_ArtilleryFire";
};
