class CfgPatches {
	class Kauk {
		weapons[] = {};
		requiredVersion = 0.1;
		author[] = { "Kaukassus" };
		authorUrl = "https://bitbucket.org/terriblecoders/arma3-kaukmod/";
		version = 0.9.0;
		versionStr = "0.9.0";
		versionAr[] = {0,9,0};

		#include "units.hpp"

		requiredAddons[] = {
			"A3_UI_F",
			"A3_UI_F_Curator",
			"A3_Functions_F",
			"A3_Functions_F_Curator",
			"A3_Modules_F",
			"A3_Modules_F_Curator"
		};
	};
};

class CfgFactionClasses {
	class Kauk {
		displayname = "Kauk";
		priority = 8;
		side = 7;
		addon = "Kauk";

		class subCategories {
			class Arsenal {
				displayName = "Arsenal";
				moduleClass = "Kauk_Module_Empty";
			};

			class Behaviours {
				displayname = "AI Behaviours";
				moduleClass = "Kauk_Module_Empty";
			};

			class Equipment {
				displayname = "Equipment";
				moduleClass = "Kauk_Module_Empty";
			};

			class SaveLoad {
				displayname = "Save/Load";
				moduleClass = "Kauk_Module_Empty";
			};

			class Spawn {
				displayName = "Spawn";
				moduleClass = "Kauk_Module_Empty";
			};

			class Teleport {
				displayName = "Teleport";
				moduleClass = "Kauk_Module_Empty";
			};

			class Util {
				displayName = "Util";
				moduleClass = "Kauk_Module_Empty";
			};

			class Developer {
				displayName = "Developer";
				moduleClass = "Kauk_Module_Empty";
			};
			class AICommands {
				displayName = "AI Commands";
				moduleClass = "Kauk_Module_Empty";
			};
			class Constraints {
				displayName = "Constraints & Properties";
				moduleClass = "Kauk_Module_Empty";
			};
		};
	};
};

class CfgVehicles {
	class Logic;
	class Module_F: Logic {
		class ModuleDescription {
			class AnyPlayer;
			class AnyBrain;
			class EmptyDetector;
		};
	};

	class Kauk_Module_base : Module_F {
		mapSize = 1;
		author = "Kaukassus";
		vehicleClass = "Modules";
		category = "Kauk";
		subCategory = "Behaviours";
		side = 7;

		scope = 1;				// Editor visibility; 2 will show it in the menu, 1 will hide it.
		scopeCurator = 1;		// Curator visibility; 2 will show it in the menu, 1 will hide it.

		displayName = "Godmode Module Base";	// Name displayed in the menu

		function = "";			// Name of function triggered once conditions are met
		functionPriority = 1;	// Execution priority, modules with lower number are executed first. 0 is used when the attribute is undefined
		isGlobal = 2;			// 0 for server only execution, 1 for remote execution on all clients upon mission start, 2 for persistent execution
		isTriggerActivated = 0;	// 1 for module waiting until all synced triggers are activated
		isDisposable = 0;		// 1 if modules is to be disabled once it's activated (i.e., repeated trigger activation won't work)
		// curatorInfoType = "RscDisplayAttributeModuleNuke";	// Menu displayed when the module is placed or double-clicked on by Zeus

		class Arguments {};
		class ModuleDescription: ModuleDescription {
			description = "Godmode Module Base";
		};
	};

	class Kauk_Arsenal_Module_Base : Kauk_Module_base {
		subCategory = "Arsenal";
	};

	class Kauk_Behaviours_Module_Base : Kauk_Module_base {
		subCategory = "Behaviours";
	};

	class Kauk_Equipment_Module_Base : Kauk_Module_base {
		subCategory = "Equipment";
	};

	class Kauk_SaveLoad_Module_Base : Kauk_Module_base {
		subCategory = "SaveLoad";
	};

	class Kauk_Spawn_Module_Base : Kauk_Module_base {
		subCategory = "Spawn";
	};

	class Kauk_Teleport_Module_Base : Kauk_Module_base {
		subCategory = "Teleport";
	};

	class Kauk_Util_Module_Base : Kauk_Module_base {
		subCategory = "Util";
	};

	class Kauk_Developer_Module_Base : Kauk_Module_base {
		subCategory = "Developer";
	};
	class Kauk_AICommands_Module_Base : Kauk_Module_base {
		subCategory = "AICommands";
	};
	class Kauk_Constraints_Module_Base : Kauk_Module_base {
		subCategory = "Constraints";
	};

	// Placeholder class that doesn't do anything. Used for generating categories in UI.
	class Kauk_Module_Empty : Kauk_Module_base {
		category = "Curator";
		subCategory = "";

		mapSize = 0;
		displayName = "Godmode Module Empty";
		function = "Kauk_fnc_Empty";
	}

	#include "cfgVehiclesModulesBehaviours.hpp"
	#include "cfgVehiclesModulesEquipment.hpp"
	#include "cfgVehiclesModulesSaveLoad.hpp"
	#include "cfgVehiclesModulesSpawn.hpp"
	#include "cfgVehiclesModulesTeleport.hpp"
	#include "cfgVehiclesModulesArsenal.hpp"
	#include "cfgVehiclesModulesUtil.hpp"

	#include "cfgVehiclesModulesDeveloper.hpp"
	#include "cfgVehiclesModulesAICommands.hpp"
	#include "cfgVehiclesModulesConstraints.hpp"
};

class CfgFunctions {
	#include "cfgFunctions.hpp"
};

#include "ui\baseDialogs.hpp"
#include "ui\copyPasteDialog.hpp"
#include "ui\dynamicDialog.hpp"
#include "ui\attachToAdjustDialog.hpp"
#include "ui\debugAtPos.hpp"
#include "ui\controlPanelDialog.hpp"
#include "ui\curatorPropertyDialog.hpp"