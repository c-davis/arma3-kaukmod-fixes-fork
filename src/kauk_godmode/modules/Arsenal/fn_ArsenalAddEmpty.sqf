#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
["AmmoboxInit", [_object, false]] call BIS_fnc_arsenal;

[objNull, "Attached Empty Virtual Arsenal to Object"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
