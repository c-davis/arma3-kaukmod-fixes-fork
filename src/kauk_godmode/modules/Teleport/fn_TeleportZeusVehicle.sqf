/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_TeleportZeusVehicle
|----------------------------------------------------------------------
|Description:
|Teleports the current Zeus into a certain vehicle slot
|
|Usage:
|Select module, then click on vehicle. A Dialogue opens up in which you
|can select which slot you want to take.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Teleport Zeus In Vehicle",
	[
		"Select Slot",
		[
    "Driver",
    "Gunner",
    "Commander",
    "Turret",
    "Cargo",
    "Any"
		]
	]] call Kauk_fnc_ShowChooseDialog;

  switch (_dialogResult select 0) do {
    case 0: {player moveInDriver _vehicle;};
    case 1: {player moveInGunner _vehicle;};
    case 2: {player moveInCommander _vehicle;};
    case 3: {player moveInTurret [_vehicle, [0,0]];};
    case 4: {player moveInCargo _vehicle;};
    case 5: {player moveInAny _vehicle;};
    default { hint "Error"; };
  };

[objNull, "Teleported Zeus to Vehicle"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
