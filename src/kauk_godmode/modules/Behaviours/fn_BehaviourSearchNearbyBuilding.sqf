#include "\kauk_godmode\module_header.hpp"

_groupUnderCursor = [_logic] call Kauk_fnc_GetGroupUnderCursor;

_codeBlock = compile preprocessFileLineNumbers '\kauk_godmode\functions\fn_SearchBuilding.sqf';
[_codeBlock, [_groupUnderCursor, 50, "NEAREST", getPos _logic, true, false, false]] call Kauk_fnc_BroadcastCode;
[objnull, "Searching nearby building."] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
