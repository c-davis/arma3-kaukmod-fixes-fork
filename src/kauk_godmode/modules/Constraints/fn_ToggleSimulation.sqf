/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_ToggleSimulation
|----------------------------------------------------------------------
|Description:
|Toggles simulation on a selected object
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Toggle Simulation",
	[
		"Toggle Simulation...",
		[
			"On",
			"Off"
		],1
	]] call Kauk_fnc_ShowChooseDialog;

_simulation = true;
switch (_dialogResult select 0) do {
  case 0: { _simulation = true; };
  case 1: { _simulation = false; };
  default { _simulation = true; };
};

fn_sim_Call = {(_this select 0) enableSimulation (_this select 1);};
[[_object, _simulation],"fn_sim_Call",True,False] call BIS_fnc_MP;

if (_simulation) then {
	[objNull, "Object is now Simulated."] call bis_fnc_showCuratorFeedbackMessage;
} else {
	[objNull, "Object is now not Simulated."] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
