/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_PushObject
|----------------------------------------------------------------------
|Description:
|Pushes an object in a certain direction with a given force
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Push Object",
	[
		["Direction", ["Up","Front","Up + Front", "Up and then Front"]],
		["Force (m/s)", ["5", "10", "15", "25", "50", "100", "200"]]
	]
] call Kauk_fnc_ShowChooseDialog;

_speed = 0;
switch (_dialogResult select 1) do {
  case 0: { _speed = 5; };
  case 1: { _speed = 10; };
  case 2: { _speed = 15; };
  case 3: { _speed = 25; };
  case 4: { _speed = 50; };
  case 5: { _speed = 100; };
  case 6: { _speed = 200; };
  default { _speed = 5; };
};

switch (_dialogResult select 0) do {
  case 0: {
    //Up
    _object setVelocity [0, 0, _speed];
  };
  case 1: {
    //Front
		_vel = velocity _object;
		_dir = direction _object;
		_object setVelocity [
			(_vel select 0) + (sin _dir * _speed),
			(_vel select 1) + (cos _dir * _speed),
			(_vel select 2)
		];
  };
  case 2: {
    //Up + Front
		_vel = velocity _object;
		_dir = direction _object;
		_object setVelocity [
			(_vel select 0) + (sin _dir * _speed),
			(_vel select 1) + (cos _dir * _speed),
			(_vel select 2) + _speed
		];
  };
  case 3: {
    //Up and then Back
		_object setVelocity [0, 0, _speed];
		[_object, _speed] spawn {
			sleep 2;
			_object = _this select 0;
			_speed = _this select 1;
			_vel = velocity _object;
			_dir = direction _object;
			_object setVelocity [
				(_vel select 0) + (sin _dir * _speed),
				(_vel select 1) + (cos _dir * _speed),
				(_vel select 2)
			];
		};
  };
  default {
    //Up
    _object setVelocity [0, 0, _speed];
  };
};

[objNull, "Pushed Object"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
