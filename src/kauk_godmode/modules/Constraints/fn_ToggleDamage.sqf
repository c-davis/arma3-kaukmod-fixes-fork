/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_ToggleDamage
|----------------------------------------------------------------------
|Description:
|Toggles Godmode on a selected object
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Toggle Damage",
	[
		"Toggle Damage...",
		[
			"On",
			"Off"
		],1
	]] call Kauk_fnc_ShowChooseDialog;

_damage = true;
switch (_dialogResult select 0) do {
  case 0: { _damage = true; };
  case 1: { _damage = false; };
  default { _damage = true; };
};

_object allowDamage _damage;

if (_damage) then {
	[objNull, "Object is now vulnerable."] call bis_fnc_showCuratorFeedbackMessage;
} else {
	[objNull, "Object is now invulnerable."] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
