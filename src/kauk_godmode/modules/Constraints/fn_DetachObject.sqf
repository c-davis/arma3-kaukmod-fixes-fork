/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_DetachObject
|----------------------------------------------------------------------
|Description:
|Detaches all attachTo attached objects
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;
detach _vehicle;
[objNull, "Detached Object"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
