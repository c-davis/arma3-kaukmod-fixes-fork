/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_DetachRopes
|----------------------------------------------------------------------
|Description:
|Detaches all Ropes from unit
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_ropes = ropes _vehicle;

{
  ropeDestroy _x;
} foreach _ropes;

[objNull, "Detached Ropes"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
