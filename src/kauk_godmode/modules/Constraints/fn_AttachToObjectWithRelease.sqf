/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_AttachToObjectWithRelease
|----------------------------------------------------------------------
|Description:
|Attaches 2 objects together, adds Addaction to release attached object
|
|Usage:
|Select module, then click on first object, then select module again
|then click on second object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_state = initialPlayer getVariable "Kauk_ModuleState";

  if (_state == 0) then {
    //New State
    targetObject = _vehicle;
    initialPlayer setVariable ["Kauk_ModuleState", 1]; //Set state to 1, AKA Waiting for second option

    [objNull, format["Set Object"]] call bis_fnc_showCuratorFeedbackMessage;
  } else {
    //Second State
    _direction = getDir targetObject;
    targetObject attachTo [_vehicle];
    targetObject setDir _direction;

    module_attachment_release_index = module_attachment_release_index + 1;

    _action_id = player addAction [ format["<t color='#edbc2c'>Release Object %1</t>", module_attachment_release_index], {

      _object = objnull;
      _action_id = 0;
      {
        if ((_x select 2) == (_this select 3)) then {
          _object = _x select 0;
          _action_id = _x select 1;
          module_attachment_release_list = module_attachment_release_list - [_x select 0, _x select 1, _x select 2];
        };
      } foreach module_attachment_release_list;
      detach _object;
      player removeAction _action_id;
    }, module_attachment_release_index];

    module_attachment_release_list pushBack [targetObject, _action_id, module_attachment_release_index];

    initialPlayer setVariable ["Kauk_ModuleState", 0]; //Set state to 0, AKA new state

    [objNull, format["Attached Object"]] call bis_fnc_showCuratorFeedbackMessage;
  };

#include "\kauk_godmode\module_footer.hpp"
