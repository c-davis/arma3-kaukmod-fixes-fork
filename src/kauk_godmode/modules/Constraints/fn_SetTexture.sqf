/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_SetTexture
|----------------------------------------------------------------------
|Description:
|Change a hiddenselection texture of given vehicle
|
|Usage:
|Select module, then click on vehicle.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Change Weather",
	[
		["Textures", ["AAF Camo", "Nato Camo", "CSAT Camo", "Digital Snow", "Black", "Dark Grey", "Light Grey", "White", "Navy", "Brown", "Woodland", "Desert Color", "Olive Drab", "Light Green", ""]],
		["Selection ID", ["0", "1", "2", "3", "4", "5", "6"]]
	]
] call Kauk_fnc_ShowChooseDialog;

_texture = "";
_selection = 0;

switch (_dialogResult select 0) do {
  case 0: { _texture = "A3\Air_F\Heli_Light_02\Data\heli_light_02_ext_indp_co.paa"; };
  case 1: { _texture = "\A3\soft_F\MRAP_01\data\MRAP_01_base_CO.paa"; };
  case 2: { _texture = "A3\Air_F_Beta\Heli_Attack_02\Data\Heli_Attack_02_body1_CO.paa"; };
  case 3: { _texture = "A3\Air_F\Heli_Light_01\Data\skins\heli_light_01_ext_digital_co.paa"; };
  case 4: { _texture = "#(argb,8,8,3)color(0.2,0.2,0.2,0.3)"; };
  case 5: { _texture = "#(argb,8,8,3)color(0.5,0.5,0.5,0.3)"; };
  case 6: { _texture = "#(argb,8,8,3)color(0.9,0.9,0.9,0.3)"; };
  case 7: { _texture = "#(argb,8,8,3)color(0.9,0.9,0.9,0.6)"; };
  case 8: { _texture = "#(argb,8,8,3)color(0.24,0.36,0.45,0.3)"; };
  case 9: { _texture = "#(argb,8,8,3)color(0.8,0.1,0.8,0.3)"; };
  case 10: { _texture = "rhsusf\addons\rhsusf_m113\data\rhsusf_m113a3_01_co.paa"; };
  case 11: { _texture = "#(argb,8,8,3)color(0.78,0.68,0.5,0.3)"; };  //Desert
	case 12: { _texture = "#(argb,8,8,3)color(0.33,0.42,0.18,0.3)"; }; //OD
	case 13: { _texture = "#(argb,8,8,3)color(0.53,0.55,0.37,0.3)"; };
	case 14: { _texture = "\kauk_godmode\camo\good.paa"; };

  default { _texture = "#(argb,8,8,3)color(0,0,0,0.3)"; };
};
switch (_dialogResult select 1) do {
  case 0: { _selection = 0; };
  case 1: { _selection = 1; };
  case 2: { _selection = 2; };
  case 3: { _selection = 3; };
  case 4: { _selection = 4; };
  case 5: { _selection = 5; };
  default { _selection = 0; };
};
_vehicle setObjectTextureGlobal [_selection, _texture];

[objNull, "Changed texture of vehicle."] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
