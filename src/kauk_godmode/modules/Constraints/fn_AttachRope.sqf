/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_AttachRope
|----------------------------------------------------------------------
|Description:
|Attaches a rope between 2 objects.
|
|Usage:
|Select module, then click on first object, then select module again
|then click on second object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_vehicle = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_state = initialPlayer getVariable "Kauk_ModuleState";

if (_state == 0) then {
  //New State
  targetObject = _vehicle;
  initialPlayer setVariable ["Kauk_ModuleState", 1]; //Set state to 1, AKA Waiting for second option
  [objNull, format["Set Object"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  //Second State

  _rope = ropeCreate [_vehicle, [0,0,0], targetObject, [0,0,0]];

  initialPlayer setVariable ["Kauk_ModuleState", 0]; //Set state to 0, AKA new state
  [objNull, "Attached Object"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
