#include "\kauk_godmode\module_header.hpp"

  _objectsToRemove = nearestObjects [(position _logic), ["All"], 6];
  {
    deleteVehicle _x;
  } forEach _objectsToRemove;

	[objNull, format["Force Removed %1 objects from world.", count _objectsToRemove]] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
