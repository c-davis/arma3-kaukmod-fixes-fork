#include "\kauk_godmode\module_header.hpp"

_location = getPos _logic;

_options = [
  ["Custom CfgWeapon", option_CfgWeapon],
  ["122mm Rocket (RHS)","rhs_ammo_m21OF_HE"],
  ["230mm HE","R_230mm_HE"],
  ["40mm HE Grenade","G_40mm_HE"],
  ["RPG42 HE","R_TBG32V_F"],
  ["DAGR","M_PG_AT"],
  ["155mm AWOS Cluster","Cluster_155mm_AMOS"],
  ["155mm AWOS HE","Sh_155mm_AMOS"],
  ["155mm AWOS Mine (AP)","Mine_155mm_AMOS_range"],
  ["155mm AWOS Mine (AT)","AT_Mine_155mm_AMOS_range"],
  ["Titan AP Missile","M_Titan_AP"],
  ["GBU 12","Bo_GBU12_LGB"],
  ["9M79B Rocket (RHS)","RHS_9M79B"],
  ["NLAW Rocket","M_NLAW_AT_F"],
  ["40mm Smoke Grenade","G_40mm_Smoke"],
  ["Unguided Bomb 500lb","Bomb_04_F"],
  ["Unguided Bomb 1000lb","Bomb_03_F"],
  ["HE Rocket","Rocket_03_HE_F"],
  ["GBU31 JDAM (F18)","js_a_fa18_GBU31_JDAM"],
  ["",""]
];

_optionNames = [];
{
	_optionNames pushBack (_x select 0);
} forEach _options;

_dialogResult = [
	"Launch Fire Support",
	[
		["Ordnance", _optionNames],
		["Amount of Ordnance", ["1", "2", "5", "8", "10", "15", "20", "50", "80"]],
		["Area of Impact", ["1m", "10m", "50m", "100m", "150m", "200m", "300m"]],
		["Delay between shots", ["0", "0.2", "0.5", "1", "2", "3", "5"]]
	]
] call Kauk_fnc_ShowChooseDialog;

if (count _dialogResult > 0) then {

	//Selected Weapon
	_ordnance = (_options select (_dialogResult select 0)) select 1;

	// Amount is -1 everywhere because For Loop below starts at 0.
	_amount = 0;
	switch (_dialogResult select 1) do {
	  case 0: { _amount = 0;};
	  case 1: { _amount = 1;};
		case 2: { _amount = 4;};
		case 3: { _amount = 7;};
		case 4: { _amount = 9;};
		case 5: { _amount = 14;};
		case 6: { _amount = 19;};
		case 7: { _amount = 49;};
		case 8: { _amount = 79;};
	  default { _amount = 0; };
	};

	_area_of_impact = 0;
	switch (_dialogResult select 2) do {
	  case 0: { _area_of_impact = 1;};
	  case 1: { _area_of_impact = 10;};
		case 2: { _area_of_impact = 50;};
		case 3: { _area_of_impact = 100;};
		case 4: { _area_of_impact = 150;};
		case 5: { _area_of_impact = 200;};
		case 6: { _area_of_impact = 300;};
	  default { _area_of_impact = 1; };
	};

	_delay = 0;
	switch (_dialogResult select 3) do {
	  case 0: { _delay = 0;};
	  case 1: { _delay = 0.2;};
		case 2: { _delay = 0.5;};
		case 3: { _delay = 1;};
		case 4: { _delay = 2;};
		case 5: { _delay = 3;};
		case 6: { _delay = 5;};
	  default { _delay = 1; };
	};

	[objNull, "Starting Artillery barrage"] call bis_fnc_showCuratorFeedbackMessage;

	for "_x" from 0 to _amount step 1 do {
		_RandomArray = [];
		for "_y" from 0 to 2 step 1 do {
			_r_neg_change = floor(random 2);
			_random = random _area_of_impact;
			_random_neg = random -_area_of_impact;
			_random_current = 0;
			if (_r_neg_change == 1) then {
				_random_current = _random;
			} else {
				_random_current = _random_neg;
			};
			_RandomArray pushBack _random_current;
		};

		_launched = _ordnance createVehicle [(_location select 0) + (_RandomArray select 0), (_location select 1) + (_RandomArray select 1), 400 + (_RandomArray select 2)];
		_launched setVectorDirAndUp [[0,0,-1],[0,1,0]];
		_speed = 1;
		_launched setVelocity [0, 0, -_speed];
		sleep _delay;
	};
};

#include "\kauk_godmode\module_footer.hpp"
