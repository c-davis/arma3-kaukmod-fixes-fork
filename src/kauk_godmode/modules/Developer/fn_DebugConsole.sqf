/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_DebugConsole
|----------------------------------------------------------------------
|Description:
|Opens up the debug menu with the slected object as (_this select 0)
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

term_obj = [_logic] call Kauk_fnc_GetUnitUnderCursor;
term_pos = getPos _logic;
_dialog = createDialog "kauk_DebugCall_Dialog";

#include "\kauk_godmode\module_footer.hpp"
