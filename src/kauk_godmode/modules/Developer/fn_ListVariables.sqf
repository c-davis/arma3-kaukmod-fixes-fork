/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_ListVariables
|----------------------------------------------------------------------
|Description:
|Copies all Variable Names to the clipboard and shows a selection
| view of every var.
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/


#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_allVars = allVariables _object;
_vars = [];
{
  _var = "";
  if (isNil {_object getVariable _x}) then {
    _var = "NULL";
  } else {
    _var = _object getVariable _x;
  };

  _vars pushBack format["%1: %2 | ",_x, _var];
} foreach _allVars;

copyToClipboard str _vars;

_dialogResult = [
  "Select variable:",
  [
      ["Variable: ",_allVars]
  ]
] call Kauk_fnc_ShowChooseDialog;

_item = _allvars select (_dialogResult select 0);

_value = "";
if (isNil {_object getVariable _item}) then {
  _value = "NULL";
} else {
  _value = _object getVariable _item;
};

hint str format["Value: %1", _value];

[objNull, "Var in hint & Clipboard."] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
