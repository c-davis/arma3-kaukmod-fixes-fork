/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_GetObjectClass
|----------------------------------------------------------------------
|Description:
|Opens a hint showing the classname of the clicked object
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/


#include "\kauk_godmode\module_header.hpp"

_debugPos = [_logic] call Kauk_fnc_GetUnitUnderCursor;

hint format ["Class Name: %1", typeof _debugPos];

copyToClipboard str (typeof _debugPos);

[objNull, "Classname in hint & Clipboard."] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
