/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_RemoveOrdnance
|----------------------------------------------------------------------
|Description:
|Ability to Remove different kinds of ordnance of the Enemy from the
|battlefield.
|
|Usage:
|Select module and click somewhere on the map and the Dialog to
|configure the settings opens.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_dialogResult = [
	"Remove Ordnance Type from faction",
	[
		["Side", ["OPFOR", "BLUFOR", "GUER", "CIV"]],
    ["Ordnance Type", ["AA Missiles", "AT/AP Missiles"]]
	]
] call Kauk_fnc_ShowChooseDialog;

if (count _dialogResult > 0) then {
  _AA_Missiles = ["launch_B_Titan_F", "launch_I_Titan_F", "launch_O_Titan_F", "launch_Titan_F", "missiles_titan", "missiles_titan_static"];
  _AT_Missiles = ["launch_B_Titan_short_F", "launch_I_Titan_short_F", "launch_O_Titan_short_F", "launch_Titan_short_F", "missiles_titan", "missiles_titan_static"];

  _faction = east;
  switch (_dialogResult select 0) do {
    case 0: { _faction = east;};
    case 1: { _faction = west;};
    case 2: { _faction = resistance;};
    case 3: { _faction = civilian;};
  };

  {
		if ((side _x) == _faction) then {
      _unit = _x;
      switch (_dialogResult select 1) do {
      	case 0: {{_unit removeWeapon _x;} forEach _AA_Missiles;}; //Remove all AA Missiles
      	case 1: {{_unit removeWeapon _x;} forEach _AT_Missiles;}; //Remove all AT Missiles
      };
    };
	} forEach allUnits;
};

#include "\kauk_godmode\module_footer.hpp"
