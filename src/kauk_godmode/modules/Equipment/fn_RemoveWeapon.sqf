/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_RemoveWeapon
|----------------------------------------------------------------------
|Description:
|Removes a Weapon and it's assigned magazines from a vehicle.
|
|Usage:
|In game, select module and click on vehicle. Then select weapon
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_weapons = (weapons _object);
_weapons_names = [];
{
  _weapons_names pushBack (getText (configfile >> "CfgWeapons" >> _x >> "displayName"));
} foreach _weapons;


_dialogResult =
	[
		"Remove Weapon from vehicle",
		[
			["Select Weapon...", _weapons_names]
		]
	] call Kauk_fnc_ShowChooseDialog;

if ((count _dialogResult) > 0) then {
  _weapon = _weapons select (_dialogResult select 0);
  _mag_array_DE = getArray (configfile >> "CfgWeapons" >> _weapon >> "magazines");					//Default Magazine config
  _mag_array_AP = getArray (configfile >> "CfgWeapons" >> _weapon >> "AP" >> "magazines");  //AP Magazines
  _mag_array_HE = getArray (configfile >> "CfgWeapons" >> _weapon >> "HE" >> "magazines");  //HE Magazines
  _mag_array_AT = getArray (configfile >> "CfgWeapons" >> _weapon >> "AT" >> "magazines");  //AT Magazines
  _mag_array = _mag_array_DE + _mag_array_AP + _mag_array_HE + _mag_array_AT;
  _object removeWeapon _weapon;

  _magazines_count = count (magazines _object);

  for [{_i=0}, {_i<_magazines_count}, {_i=_i+1}] do {
  	{
  		if (_x != "") then {
  			_object removeMagazine _x;
  		};
  	} foreach _mag_array;
  };

  [objNull, format["Weapon: %1 Removed.", _weapon]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  [objNull, "No Weapon Found"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
