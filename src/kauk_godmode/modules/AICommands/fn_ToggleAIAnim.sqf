/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_ToggleAIAnim
|----------------------------------------------------------------------
|Description:
|Toggles the AI Brain and animations.
|
|Usage:
|Select module and click on the unit which you want to control
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
	"Toggle AI Animation",
	[
		"Toggle AI Animation...",
		[
			"On",
			"Off"
		],1
	]] call Kauk_fnc_ShowChooseDialog;

_simulation = true;
switch (_dialogResult select 0) do {
  case 0: { _simulation = true; };
  case 1: { _simulation = false; };
  default { _simulation = true; };
};

if (_simulation) then {
	_object enableAI "ANIM";
} else {
	_object disableAI "ANIM";
};

if (_simulation) then {
	[objNull, "AI is now Awake."] call bis_fnc_showCuratorFeedbackMessage;
} else {
	[objNull, "AI is now Frozen."] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
