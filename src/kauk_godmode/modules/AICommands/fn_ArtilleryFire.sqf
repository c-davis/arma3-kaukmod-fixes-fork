/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_ArtilleryFire
|----------------------------------------------------------------------
|Description:
|Command Artillery to fire at given position
|
|Usage:
|Select module and click on unit which you want to fire his weapon
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_state = initialPlayer getVariable "Kauk_ModuleState";

if (_state == 0) then {
  //New State
	targetArtillery = [_logic] call Kauk_fnc_GetUnitUnderCursor;
	initialPlayer setVariable ["Kauk_ModuleState", 1]; //Set state to 1, AKA Waiting for second option
  [objNull, format["Artillers Selected, now select Target position"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  //Second State
  _pos = getPos _logic;
	_mag = getArtilleryAmmo [targetArtillery];

	_mag_names = [];
	{
		_mag_names pushBack (getText (configfile >> "CfgMagazines" >> _x >> "displayName"));
	} foreach _mag;

	//Create the GUI for the Artillery Menu
	_dialogResult = [
		"Artillery Options",
		[
			["Ammo Type: ",_mag_names],
			["Amount of rounds:", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]]
		]
	] call Kauk_fnc_ShowChooseDialog;

	_mag_selected = _mag select (_dialogResult select 0);
	_rounds = (_dialogResult select 1) + 1;
	_eta = round (targetArtillery getArtilleryETA [_pos, _mag_selected]);
	targetArtillery doArtilleryFire [_pos, _mag_selected, _rounds];
	hint format["ETA: %1 Seconds", _eta];

	initialPlayer setVariable ["Kauk_ModuleState", 0]; //Set state to 0, AKA new state
  [objNull, "Artillery firing at position"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
