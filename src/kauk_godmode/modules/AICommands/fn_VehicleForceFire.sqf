/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_VehicleForceFire
|----------------------------------------------------------------------
|Description:
|Force a vehicle to fire it's gun.
|
|Usage:
|Select module and click on vehicle to force it to fire
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_vehicle = vehicle _object;
_vehicle fireAtTarget [objNull];

[objNull, "Vehicle forced to use Current Gun."] call bis_fnc_showCuratorFeedbackMessage;


#include "\kauk_godmode\module_footer.hpp"
