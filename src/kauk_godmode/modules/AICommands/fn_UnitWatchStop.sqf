/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_WatchStop
|----------------------------------------------------------------------
|Description:
|Stop an unit from watching a position
|
|
|Usage:
|Select module and click on the unit to stop watching
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_object doWatch objNull; //Stop Watching Position
[objNull, "Unit Stops watching position"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
