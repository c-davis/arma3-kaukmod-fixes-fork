/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_UnitLookObject
|----------------------------------------------------------------------
|Description:
|Make unit look at specific object
|
|
|Usage:
|Select module and click on the unit and then click on the object
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_state = initialPlayer getVariable "Kauk_ModuleState";

if (_state == 0) then {
  //New State
  lookAtSource = _object;
  initialPlayer setVariable ["Kauk_ModuleState", 1]; //Set state to 1, AKA Waiting for second option
  [objNull, format["Set Object"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  //Second State
  lookAtSource lookAt _object;
  initialPlayer setVariable ["Kauk_ModuleState", 0]; //Set state to 0, AKA new state
  [objNull, "Object Looking at position"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
