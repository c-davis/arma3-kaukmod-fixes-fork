class TeleportModules {
	file = "\kauk_godmode\modules\Teleport";
	class TeleportAllPlayers;
	class TeleportGroup;
	class TeleportSide;
	class TeleportSinglePlayer;
	class TeleportZeus;
	class TeleportZeusVehicle;
};
