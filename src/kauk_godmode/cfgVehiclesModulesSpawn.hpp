class Kauk_Module_Force_Remove_Area : Kauk_Spawn_Module_Base {
	scopeCurator = 2;
	displayName = "Force Remove Area";
	function = "Kauk_fnc_ForceRemoveArea";
};

class Kauk_Module_Force_Remove_Object : Kauk_Spawn_Module_Base {
	scopeCurator = 2;
	displayName = "Force Remove Object";
	function = "Kauk_fnc_ForceRemoveObject";
};

class Kauk_Module_Spawn_LHD : Kauk_Spawn_Module_Base {
	scopeCurator = 2;
	displayName = "Spawn LHD";
	function = "Kauk_fnc_SpawnLHD";
};

class Kauk_Module_SpawnMenuAdvanced: Kauk_Spawn_Module_Base {
	scopeCurator = 2;
	displayName = "Spawn Menu Advanced";
	function = "Kauk_fnc_SpawnMenuAdvanced";
};

class Kauk_Module_SpawnSupportModule: Kauk_Spawn_Module_Base {
	scopeCurator = 2;
	displayName = "Spawn Support Module";
	function = "Kauk_fnc_SpawnSupportModule";
};
