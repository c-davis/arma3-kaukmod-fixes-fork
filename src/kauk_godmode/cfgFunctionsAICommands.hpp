class AICommandsModules {
	file = "\kauk_godmode\modules\AICommands";
	class ToggleAIAnim;
	class UnitForceFire;
	class UnitLook;
	class UnitLookObject;
	class SelectAIWeapon;
	class VehicleForceFire;
	class UnitWatch;
	class UnitWatchStop;
	class ArtilleryFire;
};
