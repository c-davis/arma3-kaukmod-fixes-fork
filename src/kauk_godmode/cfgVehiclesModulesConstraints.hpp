class Kauk_Module_Constraints_Set_Texture : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Set Vehicle Texture";
	function = "Kauk_fnc_SetTexture";
};

class Kauk_Module_Constraints_Attach_To : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Attach Object";
	function = "Kauk_fnc_AttachToObject";
};

class Kauk_Module_Constraints_Detach : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Detach Object";
	function = "Kauk_fnc_DetachObject";
};

class Kauk_Module_Constraints_Attach_Rope : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Attach Rope";
	function = "Kauk_fnc_AttachRope";
};

class Kauk_Module_Constraints_Detach_Rope : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Detach Rope";
	function = "Kauk_fnc_DetachRopes";
};

class Kauk_Module_Constraints_Toggle_Damage : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Toggle Damage";
	function = "Kauk_fnc_ToggleDamage";
};

class Kauk_Module_Constraints_Push_Object : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Push Object";
	function = "Kauk_fnc_PushObject";
};

class Kauk_Module_Constraints_UnlimitedAmmo : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Unlimited Ammo";
	function = "Kauk_fnc_UnlimitedAmmo";
};

class Kauk_Module_Constraints_ToggleSimulation : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Toggle Simulation";
	function = "Kauk_fnc_ToggleSimulation";
};

class Kauk_Module_Constraints_Attach_With_Release : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Attach Object Rel";
	function = "Kauk_fnc_AttachToObjectWithRelease";
};

class Kauk_Module_Constraints_ChangePlayerSides : Kauk_Constraints_Module_Base {
	scopeCurator = 2;
	displayName = "Change Player Sides";
	function = "Kauk_fnc_ChangePlayerSides";
};
