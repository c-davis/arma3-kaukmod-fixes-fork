class BehaviourModules {
	file = "\kauk_godmode\modules\Behaviours";
	class BehaviourPatrol;
	class BehaviourSearchNearbyAndGarrison;
	class BehaviourSearchNearbyBuilding;
	class GarrisonNearest;
	class UnGarrison;
};
