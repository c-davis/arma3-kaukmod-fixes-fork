
class Kauk_Module_Add_All_Objects_To_Curator : Kauk_Util_Module_Base {
	scopeCurator = 2;
	displayName = "Add Objects To Zeus";
	function = "Kauk_fnc_AddAllObjectsToCuratorModule";
};

class Kauk_Module_Util_Change_Weather : Kauk_Util_Module_Base {
	scopeCurator = 2;
	displayName = "Change Weather";
	function = "Kauk_fnc_UtilSetWeatherModule";
};

class Kauk_Module_Remove_Objects_From_Curator : Kauk_Util_Module_Base {
	scopeCurator = 2;
	displayName = "Remove Objects From Zeus";
	function = "Kauk_fnc_RemoveObjectsFromCuratorModule";
};

class Kauk_Module_Util_Launch_Strike : Kauk_Util_Module_Base {
	scopeCurator = 2;
	displayName = "Launch Strike";
	function = "Kauk_fnc_LaunchStrike";
};
