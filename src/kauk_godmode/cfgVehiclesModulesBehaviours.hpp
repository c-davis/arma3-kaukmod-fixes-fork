class Kauk_Module_Garrison_Nearest : Kauk_Behaviours_Module_Base {
	scopeCurator = 2;
	displayName = "Garrison Building (instant)";
	function = "Kauk_fnc_GarrisonNearest";
};

class Kauk_Module_UnGarrison : Kauk_Behaviours_Module_Base {
	scopeCurator = 2;
	displayName = "Un-Garrison";
	function = "Kauk_fnc_UnGarrison";
};

class Kauk_Module_Behaviour_Search_Nearby_Building : Kauk_Behaviours_Module_Base {
	scopeCurator = 2;
	displayName = "Search Building";
	function = "Kauk_fnc_BehaviourSearchNearbyBuilding";
};

class Kauk_Module_Behaviour_Search_Nearby_And_Garrison : Kauk_Behaviours_Module_Base {
	scopeCurator = 2;
	displayName = "Search And Garrison Building";
	function = "Kauk_fnc_BehaviourSearchNearbyAndGarrison";
};

class Kauk_Module_Behaviour_Patrol : Kauk_Behaviours_Module_Base {
	scopeCurator = 2;
	displayName = "Patrol";
	function = "Kauk_fnc_BehaviourPatrol";
};
