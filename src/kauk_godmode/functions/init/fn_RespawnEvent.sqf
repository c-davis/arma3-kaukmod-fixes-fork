/*
|----------------------------------------------------------------------
|Name: respawn_event.sqf
|----------------------------------------------------------------------
|Description:
|Script file to be used for the respawn event handler which adds
|back Zeus, and the Control panel.
|
|Usage:
|Tied to Respawn eventhandler of player
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

//Constants
#define PLAYER_ADD_AS_ZEUSOBJECT true
#define PLAYER_ALLOW_CONTROLPANEL true

call Kauk_fnc_ZeusRefresh;

//Initial Player is always the real player body. This is in respawn so the respawned body gets properly registered again.
initialPlayer = player;

//State Settings
initialPlayer setVariable ["Kauk_ModuleState", 0];           // Module status variable for modules with multiple statuses
initialPlayer setVariable ["Kauk_CP_IsInvinsible", false];   // Player is invinsible
initialPlayer setVariable ["Kauk_CP_IsGod", false];          // Player has Godmode
initialPlayer setVariable ["Kauk_CP_ReviveActivated", false];// Player has Respawn
initialPlayer setVariable ["Kauk_CP_Fatigue", true];         // Fatigue state of player
initialPlayer setVariable ["Kauk_CP_Grass", true];         // Grass state of player
initialPlayer setVariable ["Kauk_CP_LeaderMove", true];      // Is AI Leader allowed to move?

//ACE Specific Settings:
player setVariable ["ace_medical_medicClass", 1]; // Make Player Medic

//Add player as Zeus Editable object
if (PLAYER_ADD_AS_ZEUSOBJECT) then {
  [[player]] call Kauk_fnc_AddUnitsToCurator;
};