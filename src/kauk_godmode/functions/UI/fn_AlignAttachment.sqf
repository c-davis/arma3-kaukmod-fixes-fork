_type = [_this, 0] call BIS_fnc_Param;
_amount_roll = 0.5;
_o = kauk_curatorPropertyObject;

//Travelling distance of setPosATL
_amount_distance = parseNumber (ctrlText 1401);

//Direction degrees of setDir
_amount_dir_rel =  parseNumber (ctrlText 1400);

//Absolute Degrees of setDir
_amount_dir_abs =  parseNumber (ctrlText 1402);

//Variable Check
if (isNil {_o getVariable "Adjust_Slider_X"}) then {
  _o setVariable ["Adjust_Slider_X", 0];
};
if (isNil {_o getVariable "Adjust_Slider_Y"}) then {
  _o setVariable ["Adjust_Slider_Y", 0];
};
if (isNil {_o getVariable "Adjust_Slider_Z"}) then {
  _o setVariable ["Adjust_Slider_Z", 0];
};

_xa = _o getVariable "Adjust_Slider_X";
_ya = _o getVariable "Adjust_Slider_Y";
_za = _o getVariable "Adjust_Slider_Z";
_angle_amount = _xa + _ya + _za;

switch (_type) do {
  case 0: {
    // Front
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1) + _amount_distance, ((getPosATL _o) select 2)];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1) + _amount_distance, ((getPosATL _o) select 2)];
      _o setDir _direction;
    };

  };
  case 1: {
    // Back
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1) - _amount_distance, ((getPosATL _o) select 2)];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1) - _amount_distance, ((getPosATL _o) select 2)];
      _o setDir _direction;
    };

  };
  case 2: {
    // Left
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0) - _amount_distance, ((getPosATL _o) select 1), ((getPosATL _o) select 2)];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0) - _amount_distance, ((getPosATL _o) select 1), ((getPosATL _o) select 2)];
      _o setDir _direction;
    };

  };
  case 3: {
    // Right
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0) + _amount_distance, ((getPosATL _o) select 1), ((getPosATL _o) select 2)];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0) + _amount_distance, ((getPosATL _o) select 1), ((getPosATL _o) select 2)];
      _o setDir _direction;
    };

  };
  case 4: {
    // Up
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1), ((getPosATL _o) select 2) + _amount_distance];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1), ((getPosATL _o) select 2) + _amount_distance];
      _o setDir _direction;
    };

  };
  case 5: {
    // Down
    if (_angle_amount > 0) then {
      [0, 0, 0] call Kauk_fnc_SetAngle;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1), ((getPosATL _o) select 2) - _amount_distance];
      [_xa, _ya, _za] call Kauk_fnc_SetAngle;
    } else {
      _direction = getDir _o;
      _o setDir 0;
      _o setPosATL [((getPosATL _o) select 0), ((getPosATL _o) select 1), ((getPosATL _o) select 2) - _amount_distance];
      _o setDir _direction;
    };

  };
  case 6: {
    // Toggle Simulation
    fn_sim_Call = {(_this select 0) enableSimulation (_this select 1);};
    if (simulationEnabled _o) then {
      [West,"HQ"] sideChat "Deativated: Simulation for Object";
      [[_o, false],"fn_sim_Call",True,False] call BIS_fnc_MP;
    } else {

      [West,"HQ"] sideChat "Activated: Simulation for Object";
      [[_o, true],"fn_sim_Call",True,False] call BIS_fnc_MP;
    };
  };
  case 7: {
    // Set Angle
    _angle_x = parseNumber (ctrlText 1403);
    _angle_y = parseNumber (ctrlText 1404);
    _angle_z = parseNumber (ctrlText 1405);

    [_angle_x, _angle_y, _angle_z] call Kauk_fnc_SetAngle;

    //Set Angles
    _o setVariable ["Adjust_Slider_X", _angle_x];
    _o setVariable ["Adjust_Slider_Y", _angle_y];
    _o setVariable ["Adjust_Slider_Z", _angle_z];

    sliderSetPosition [1900, _angle_x / 36];
    sliderSetPosition [1901, _angle_y / 36];
    sliderSetPosition [1902, _angle_z / 36];
  };
  case 8: {
    // Angle Slider X
    _angle_x = 36 * (_this select 1);
    _angle_y = _o getVariable "Adjust_Slider_Y";
    _angle_z = _o getVariable "Adjust_Slider_Z";

    [_angle_x, _angle_y, _angle_z] call Kauk_fnc_SetAngle;

    _o setVariable ["Adjust_Slider_X", _angle_x];
    ctrlSetText [1403, str _angle_x];
  };
  case 9: {
    // Angle Slider Y
    _angle_x = _o getVariable "Adjust_Slider_X";
    _angle_y = 36 * (_this select 1);
    _angle_z = _o getVariable "Adjust_Slider_Z";

    [_angle_x, _angle_y, _angle_z] call Kauk_fnc_SetAngle;

    _o setVariable ["Adjust_Slider_Y", _angle_y];
    ctrlSetText [1404, str _angle_y];
  };
  case 10: {
    // Angle Slider Z
    _angle_x = _o getVariable "Adjust_Slider_X";
    _angle_y = _o getVariable "Adjust_Slider_Y";
    _angle_z = 36 * (_this select 1);

    [_angle_x, _angle_y, _angle_z] call Kauk_fnc_SetAngle;

    _o setVariable ["Adjust_Slider_Z", _angle_z];
    ctrlSetText [1405, str _angle_z];
  };
  default {
    // Default
    hint "Out Of Array error happened.";
  };
};
