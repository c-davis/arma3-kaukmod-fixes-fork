/*

0 = Local
1 = Server
2 = Broadcast

*/
_type = [_this, 0] call BIS_fnc_Param;
_code = ctrlText 1400;

switch (_type) do {
  case 0: {[] call compile _code;};
  case 1: {[(compile _code), [], false] call Kauk_fnc_BroadcastCode;};
  case 2: {[(compile _code), [], true] call Kauk_fnc_BroadcastCode;};
};
