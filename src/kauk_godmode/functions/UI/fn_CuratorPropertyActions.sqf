_type = [_this, 0] call BIS_fnc_Param;

switch (_type) do {
  case "RemoteControl": {
    closeDialog 1;
    [kauk_curatorPropertyObject] call Kauk_fnc_RemoteControl;
  };
  case "SwitchPlayer": {
    closeDialog 1;
    [kauk_curatorPropertyObject] call Kauk_fnc_SwitchPlayer;
  };
  case "RemoteArsenal": {
    _unit = kauk_curatorPropertyObject;
    _error = "";
    switch true do {
      case (isnull _unit): {_error = localize "str_a3_BIS_fnc_showCuratorFeedbackMessage_506";};
      case !(alive _unit): {_error = localize "str_a3_BIS_fnc_moduleArsenal_errorDead";};
      case (isnull group _unit || !(side group _unit in [east,west,resistance,civilian])): {_error = localize "str_a3_BIS_fnc_moduleArsenal_errorBrain";};
      case (vehicle _unit != _unit || effectivecommander _unit != _unit): {_error = localize "str_a3_BIS_fnc_moduleArsenal_errorVehicle";};
    };
    if (_error == "") then {
      closeDialog 1;
      if (isClass (configfile >> "CfgPatches" >> "XLA_FixedArsenal")) then {
        ["Open",[true,nil,_unit]] spawn xla_fnc_arsenal;
      } else {
        ["Open",[true,nil,_unit]] spawn BIS_fnc_arsenal;
      };
      (finddisplay 312) closedisplay 2;
    } else {
  		[objnull,_error] call bis_fnc_showCuratorFeedbackMessage;
  	};
  };
  case "AdjustPosition": {
    createDialog "kauk_panel_adjustObject";
  };
};