_type = [_this, 0] call BIS_fnc_Param;

switch (_type) do {
  case 0: {
    //Open Arsenal
    if (isClass (configfile >> "CfgPatches" >> "XLA_FixedArsenal")) then {
      ["Open",true] spawn xla_fnc_arsenal;
    } else {
      ["Open",true] spawn BIS_fnc_arsenal;
    };
    closeDialog 2; //Close Dialog
  };
  case 1: {
    //Assign Zeus
    call Kauk_fnc_ZeusRefresh;
  };
  case 2: {
    //Open Debug Console
    _dialog = createDialog "kauk_DebugCall_Dialog";
  };
  case 3: {
    //Toggle Invinsibility
    _state = player getVariable "Kauk_CP_IsInvinsible";
    if (_state) then {
      [player, false] spawn Kauk_fnc_MakePlayerInvisible;
      player setVariable ["Kauk_CP_IsInvinsible", false];
      [West,"HQ"] sideChat "Deactivated: Invinsibility";
    } else {
      [player, true] spawn Kauk_fnc_MakePlayerInvisible;
      player setVariable ["Kauk_CP_IsInvinsible", true];
      [West,"HQ"] sideChat "Activated: Invinsibility";
    };
  };
  case 4: {
    //Toggle Godmode
    _state = player getVariable "Kauk_CP_IsGod";
    if (_state) then {
      player allowDamage true;
      player setVariable ["Kauk_CP_IsGod", false];
      [West,"HQ"] sideChat "Deactivated: Godmode";
    } else {
      player allowDamage false;
      player setVariable ["Kauk_CP_IsGod", true];
      [West,"HQ"] sideChat "Activated: Godmode";
    };
  };
  case 5: {
    //Toggle Revive System
    _state = player getVariable "Kauk_CP_ReviveActivated";
    if (_state) then {
      [false] call Kauk_fnc_ToggleReviveSystem;
      player setVariable ["Kauk_CP_ReviveActivated", false];
      [West,"HQ"] sideChat "Deactivated: Revive System";
    } else {
      [true] call Kauk_fnc_ToggleReviveSystem;
      player setVariable ["Kauk_CP_ReviveActivated", true];
      [West,"HQ"] sideChat "Activated: Revive System";
    };
  };
  case 6: {
    //Toggle Fatigue
    _fatigue = initialPlayer getVariable "Kauk_CP_Fatigue";

    if (_fatigue) then {
      initialPlayer setVariable ["Kauk_CP_Fatigue", false];
      {
        _x enableFatigue false;
      } forEach units group player;
      [West,"HQ"] sideChat "Deactivated: Fatigue";
    } else {
      initialPlayer setVariable ["Kauk_CP_Fatigue", true];
      {
        _x enableFatigue true;
      } forEach units group player;
      [West,"HQ"] sideChat "Activated: Fatigue";
    };
  };
  case 7: {
    //Toggle AI Leader Move
    _leaderstate = initialPlayer getVariable "Kauk_CP_LeaderMove";
    _groupleader = leader (group player);
    if (_leaderstate) then {
      initialPlayer setVariable ["Kauk_CP_LeaderMove", false];
      _groupleader disableAI "MOVE";
      [West,"HQ"] sideChat "Deactivated: AI Leader Movement";
    } else {
      initialPlayer setVariable ["Kauk_CP_LeaderMove", true];
      _groupleader enableAI "MOVE";
      [West,"HQ"] sideChat "Activated: AI Leader Movement";
    };
  };
  case 8: {
    //Toggle Grass
    _grass = initialPlayer getVariable "Kauk_CP_Grass";

    if (_grass) then {
      initialPlayer setVariable ["Kauk_CP_Grass", false];
      setTerrainGrid 12.5;
      [West,"HQ"] sideChat "Deactivated: Grass";
    } else {
      initialPlayer setVariable ["Kauk_CP_Grass", true];
      setTerrainGrid 50;
      [West,"HQ"] sideChat "Activated: Grass";
    };
  };
  case 9: {
    //Return to Initial Body
    player removeEventHandler ["HandleDamage", controlTargetDamageHandlerEH];
    selectPlayer initialPlayer;
    initialPlayer enableAI "ANIM";
    initialPlayer setCaptive false;
    initialPlayer allowDamage true;
    initialPlayer forceSpeed 1;
    [West,"HQ"] sideChat "Returning to Initial Body";
    [] spawn {
      sleep 1;
      call Kauk_fnc_ZeusRefresh;
    };
  };
  default {
    // Default
    hint "Out Of Array error happened.";
  };
};
