/*
	Gets the unit under the mouse cursor.

	Params:
		0 - [Object] The module's logic unit that is trying to get the unit under the cursor.
		1 - Boolean - True to remove the object passed in [0] if there is no unit under the cursor. False to leave it. (Default: True)
	Returns:
		The unit under the cursor (if any). Otherwise the logic unit parameter is deleted.
*/
private ["_logic", "_unitUnderCursor"];
_logic = _this select 0;
_shouldRemoveLogicIfNoUnitFound = [_this, 1, true] call BIS_fnc_Param;

_unitUnderCursor = objNull;

// We use the value we set when the last object was created. This is because when we call this
// (inside the module's logic) the object under the mouse is always going to be the newly created
// logic module, and not the actual object that is underneath it. We want to know what the object
// was under the mouse when the module itself was created - under the assumption that the last object
// created was the module, and that the mouse was actually on the object we want when the event
// got fired.
// TODO we could eliminate this issue if we always deleted the logic BEFORE we tried to get
// the item under the cursor.
if (!isNil "Kauk_CuratorObjectPlaced_UnitUnderCursor") then
{
	_mouseOverUnit = Kauk_CuratorObjectPlaced_UnitUnderCursor;
	if (count _mouseOverUnit != 0) then {
		if (_mouseOverUnit select 0 != "") then {
			if (count _mouseOverUnit == 2) then {
				if (_mouseOverUnit select 0 == "OBJECT") then {
					// value should be [typeName, object]
					_unitUnderCursor = _mouseOverUnit select 1;
				};
			};
		};
	};
};

if (_shouldRemoveLogicIfNoUnitFound && isNull _unitUnderCursor) then {
	deleteVehicle _logic;
};

_unitUnderCursor;
