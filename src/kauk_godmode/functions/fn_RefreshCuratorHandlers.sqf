/*
  This function removes all Curator Related EventHandlers of this Addon and reapplies them.
  This is needed in case an eventhandler gets lost, so it can be reapplied again.

*/

{
  if ((getassignedcuratorunit _x) == player) then {
    _x removeEventHandler ["CuratorObjectDoubleClicked", curatorObjectDoubleClickedEH];
    _x removeEventHandler ["CuratorObjectPlaced", curatorObjectPlacedEH];
    curatorObjectDoubleClickedEH = _x addEventHandler ["CuratorObjectDoubleClicked", { _this call fn_CuratorDoubleClick; }];
    curatorObjectPlacedEH = _x addEventHandler ["CuratorObjectPlaced", { _this call Kauk_fnc_HandleCuratorObjectPlaced; }];
  };
} foreach allCurators;
