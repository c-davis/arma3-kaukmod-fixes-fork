//True  == Turn On
//False == Turn Off

//Eventhandler Function
FP_UnconciousHandler = {
  _amountOfDamage = _this select 2;
  if (_amountOfDamage != 0) then {
    if (alive initialPlayer && _amountOfDamage >= 1) then {
      initialPlayer setDamage 0;
      initialPlayer allowDamage false;
      initialPlayer setCaptive true;
      _amountOfDamage = 0;
      [] spawn FP_UnconciousFunction;
    };
  };
  _amountOfDamage
};

//Function for being unconcious
FP_UnconciousFunction = {
  initialPlayer setDamage 0;
  findDisplay 312 closeDisplay 2;
  disableUserInput true;

  //Remove Dude from vehicle if he's in one.
  if (vehicle initialPlayer != initialPlayer) then
  {
    unAssignVehicle initialPlayer;
    initialPlayer action ["eject", vehicle initialPlayer];
    sleep 2;
  };
  initialPlayer playMove "AinjPpneMstpSnonWrflDnon_rolltoback";
  sleep 3;
  disableUserInput false;

  //Initiate Respawn
  [] spawn FP_UnconciousRespawn;
};

//Function that initiates the respawn (This may be merged with the function above)
FP_UnconciousRespawn = {
  initialPlayer linkItem "ItemMap";
  openmap true;
  onMapSingleClick {
    initialPlayer setPos _pos;
    openMap [false, false];
    onMapSingleClick {};
	};
  while {visibleMap} do {};
  initialPlayer setDamage 0;
  initialPlayer allowDamage true;
  initialPlayer setCaptive false;
  initialPlayer playMove "amovppnemstpsraswrfldnon";
  initialPlayer playMove "";
  [initialPlayer, -1] call ace_medical_fnc_adjustPainLevel
};

if (_this select 0) then {
  FP_UnconciousHandlerID = initialPlayer addEventHandler ["HandleDamage", FP_UnconciousHandler];
} else {
  initialPlayer removeEventHandler ["HandleDamage", FP_UnconciousHandlerID];
};
