
//If Unit gets "Killed", port player back first.
controlTargetDamageHandler = {
  _unit = _this select 0;
  _amountOfDamage = _this select 2;
  if (_amountOfDamage != 0) then {
    if (alive _unit && _amountOfDamage >= 1) then {
      _unit playMove "AinjPpneMstpSnonWrflDnon_rolltoback";
      _unit setDamage 0;
      _unit allowDamage false;
      _unit setCaptive true;
      _amountOfDamage = 0;
      [] spawn {
        selectPlayer initialPlayer;
        initialPlayer enableAI "ANIM";
        initialPlayer setCaptive false;
        initialPlayer allowDamage true;
        initialPlayer forceSpeed 1;
        sleep 1;
        call Kauk_fnc_ZeusRefresh;
        if (alive _unit) then {
          _unit allowDamage true;
          _unit setCaptive false;
          _unit setDamage 1;
        };
      };
    };
  };
  _amountOfDamage
};

[_this select 0] spawn {
      _unit = _this select 0;
      _leader = false;
      if (true) then {
        _vehicle = vehicle _unit;

        _mainSeatArray = [
          format ["Driver: %1", driver _vehicle],
          format ["Gunner: %1", gunner _vehicle],
          format ["Commander: %1", commander _vehicle]
        ];

        _dialogResult = [
          "Remote control Slot",
          [
            ["Position", _mainSeatArray],
            ["Squad Leader?", ["No", "Yes"]]
          ]
        ] call Kauk_fnc_ShowChooseDialog;

        switch (_dialogResult select 0) do {
          case 0: { _unit = driver _vehicle; };     //Driver
          case 1: { _unit = gunner _vehicle; };     //Gunner
          case 2: { _unit = commander _vehicle; };  //Commander
          default { _unit = driver _vehicle; };
        };

        _leader = false;
        switch (_dialogResult select 1) do {
          case 0: { _leader = false; };
          case 1: { _leader = true; };
          default { _leader = false; };
        };
      };

      //--- Check if the unit is suitable
      _error = "";
      if !(side group _unit in [east,west,resistance,civilian]) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorEmpty";};
      if (isplayer _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorPlayer";};
      if !(alive _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorDestroyed";};
      if (isnull _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorNull";};
      if !(isnull (_unit getvariable ["bis_fnc_moduleRemoteControl_owner",objnull])) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorControl";};

      if (_error == "") then {
          [_unit, _leader] spawn {
              scriptname "bis_fnc_moduleRemoteControl: Loop";
              _unit = _this select 0;
              _vehicle = vehicle _unit;
              _vehicleRole = str assignedvehiclerole _unit;

              //--- Wait for interface to close
              (finddisplay 312) closedisplay 2;
              waituntil {isnull curatorcamera};

              initialPlayer disableAI "ANIM";     //Stop AI from moving around once we are in a different body
              initialPlayer setCaptive true;       //Source is set to captive, so it's not gonna get shot at while we're in another body
              initialPlayer allowDamage false;  //Source doesen't die if it's controlling another body
              initialPlayer forceSpeed 0;          //Speed Limited when Driver

              _unit enableAI "ANIM";
              _unit setCaptive false;
              _unit allowDamage true;
              _unit forceSpeed 1;
              controlTargetDamageHandlerEH = _unit addEventHandler ["HandleDamage", controlTargetDamageHandler];

              if (_this select 1) then {(group _unit) selectLeader _unit;};
              selectPlayer  _unit;
          };
      } else {
          [objnull,_error] call bis_fnc_showCuratorFeedbackMessage;
      };
};

