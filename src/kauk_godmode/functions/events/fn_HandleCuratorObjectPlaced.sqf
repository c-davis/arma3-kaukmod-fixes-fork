private ["_curator","_placedObject"];
_curator = _this select 0;
_placedObject = _this select 1;
if (local _placedObject) then {
	Kauk_CuratorObjectPlaced_UnitUnderCursor = curatorMouseOver;
	Kauk_CuratorObjectPlaces_LastPlacedObjectPosition = position _placedObject;
};
