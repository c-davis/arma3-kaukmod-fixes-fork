class EquipmentModules {
	file = "\kauk_godmode\modules\Equipment";
	class EquipmentLights;
	class EquipmentNvgs;
	class EquipmentThermals;
	class RemoveOrdnance;
	class RemoveCargo;
	class RemoveInventory;
	class AddWeapon;
	class RemoveWeapon;
};
