
class kauk_curatorPropertyDialog
{
	idd = 199648;
	movingEnable = true;
	onLoad = "setMousePosition [0.3, 0.3];";
	class controls {
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by Baguette, v1.063, #Fucocu)
		////////////////////////////////////////////////////////

		class kauk_back_01: IGUIBack
		{
			idc = 2200;
			moving = 1;

			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 10.5 * GUI_GRID_W;
			h = 12.5 * GUI_GRID_H;
		};
		class kauk_button_remoteControl: RscButton
		{
			idc = 1600;
			onButtonClick = "_o = ""RemoteControl"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Remote Control"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 2 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_button_selectPlayer: RscButton
		{
			idc = 1601;
			onButtonClick = "_o = ""SwitchPlayer"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Select as Player"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 4 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_button_remoteArsenal: RscButton
		{
			idc = 1602;
			onButtonClick = "_o = ""RemoteArsenal"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Remote Arsenal"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 6 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_back_title: IGUIBack
		{
			idc = 2201;

			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 0.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 10.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			colorBackground[] = {0.77,0.51,0.08,1};
		};
		class kauk_button_adjustObject: RscButton
		{
			idc = 1600;
			onButtonClick = "_o = ""AdjustPosition"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Adjust Object"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 8 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_button_placeholder1: RscButton
		{
			idc = 1600;
			onButtonClick = "_o = ""AdjustPosition"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Placeholder 1"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 10 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_button_placeholder2: RscButton
		{
			idc = 1600;
			onButtonClick = "_o = ""AdjustPosition"" call Kauk_fnc_CuratorPropertyActions;";

			text = "Placeholder 2"; //--- ToDo: Localize;
			x = 1.5 * GUI_GRID_W + GUI_GRID_X;
			y = 12 * GUI_GRID_H + GUI_GRID_Y;
			w = 9.5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class kauk_text_title: RscText
		{
			idc = 1000;
			text = "Object Properties:"; //--- ToDo: Localize;
			x = 1 * GUI_GRID_W + GUI_GRID_X;
			y = 0.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			sizeEx = 0.8 * GUI_GRID_H;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////
	};
};