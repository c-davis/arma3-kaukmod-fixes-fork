class kauk_controlpanel_admin
{
	idd = 199646;
	movingEnable = true;
	onLoad = "setMousePosition [0.3, 0.3];";

	class controls
	{
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by mburgherr, v1.063, #Rokoqi)
////////////////////////////////////////////////////////

class kauk_back_01: IGUIBack
{
	idc = 2200;
	moving = 1;

	x = 1 * GUI_GRID_W + GUI_GRID_X;
	y = 0.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 11.5 * GUI_GRID_W;
	h = 21.5 * GUI_GRID_H;
};
class kauk_button_arsenal: RscButton
{
	idc = 1600;
	onButtonClick = "_o = 0 call Kauk_fnc_ControlPanelActions;";

	text = "Virtual Arsenal"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_debug_console: RscButton
{
	idc = 1601;
	onButtonClick = "_o = 2 call Kauk_fnc_ControlPanelActions;";

	text = "Open Debug Console"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_assign_zeus: RscButton
{
	idc = 1602;
	onButtonClick = "_o = 1 call Kauk_fnc_ControlPanelActions;";

	text = "Assign Zeus"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_invinsible: RscButton
{
	idc = 1603;
	onButtonClick = "_o = 3 call Kauk_fnc_ControlPanelActions;";

	text = "Toggle Invinsibility"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_godmode: RscButton
{
	idc = 1604;
	onButtonClick = "_o = 4 call Kauk_fnc_ControlPanelActions;";

	text = "Toggle Godmode"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_togglerevive: RscButton
{
	idc = 1605;
	onButtonClick = "_o = 5 call Kauk_fnc_ControlPanelActions;";

	text = "Toggle Respawn"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_fatigue_on: RscButton
{
	idc = 1606;
	onButtonClick = "_o = 6 call Kauk_fnc_ControlPanelActions;";

	text = "Toggle Fatigue"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_fatigue_off: RscButton
{
	idc = 1607;
	onButtonClick = "_o = 7 call Kauk_fnc_ControlPanelActions;";

	text = "Toggle Leader Move"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 11.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_grass_toggle: RscButton
{
	idc = 1610;
	onButtonClick = "_o = 8 call Kauk_fnc_ControlPanelActions;";

	text = "Grass On"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 13.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
class kauk_button_return_player: RscButton
{
	idc = 1611;
	onButtonClick = "_o = 9 call Kauk_fnc_ControlPanelActions;";

	text = "Return Player"; //--- ToDo: Localize;
	x = 2 * GUI_GRID_W + GUI_GRID_X;
	y = 15.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 9.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////
	};
};
