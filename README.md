![Logo](https://bytebucket.org/terriblecoders/arma3-kaukmod/raw/adec8be7aa6367605e753d92060d613d15e4f3a2/doc/res/img/kaukmod.png)

##About
Kaukmod is a set of Personal Modifications and patches I made to suit my own needs.

These modifications and patches have been tailored to my own needs and you are free to make use of anything you see in here according to the license provided in each of those modifications.

If you need informations about each one of those modules, you can check out the doc/addon-informations folder. In there, you'll find the individual licenses of each addon/pbo, descriptions and requirements.

If no License for an individual addon is mentioned, the MIT License is assumed. For a copy of each license, check out the doc/licenses folder.
